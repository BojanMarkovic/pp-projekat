package rs.ac.bg.etf.pp1;

import java.util.LinkedList;
import java.util.Stack;

import rs.ac.bg.etf.pp1.CounterVisitor.*;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;

public class CodeGenerator extends VisitorAdaptor {
	private int mainPc;
	private Obj constIncDec = null;
	private Stack<LinkedList<Integer>> fixingFalse = new Stack<LinkedList<Integer>>();
	private Stack<LinkedList<Integer>> fixingTrue = new Stack<LinkedList<Integer>>();
	private Stack<Integer> conditionStck = new Stack<Integer>();
	private Stack<Integer> Designator2Stck = new Stack<Integer>();
	private Stack<LinkedList<Integer>> stackBreak = new Stack<LinkedList<Integer>>();
	private Stack<LinkedList<Integer>> stackContinue = new Stack<LinkedList<Integer>>();

	public int getMainPc() {
		return mainPc;
	}
	
	private void specialFunction() {
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.put(Code.load_n + 0);
		Code.put(Code.exit);
		Code.put(Code.return_);
	}

	public void visit(ProgName progName) {
		constIncDec = MyTab.insert(Obj.Con, "one", MyTab.intType);
		constIncDec.setLevel(0);
		constIncDec.setAdr(1);
		Obj charMethod = MyTab.find("chr");
		charMethod.setAdr(Code.pc);
		Obj ordMethod = MyTab.find("ord");
		ordMethod.setAdr(Code.pc);
		specialFunction();
		Obj lenMethod = MyTab.find("len");
		lenMethod.setAdr(Code.pc);
		specialFunction();
	}

	public void visit(MatchedPrint matchedPrint) {
		if (matchedPrint.getExpr().struct == MyTab.charType) {
			Code.loadConst(2);
			Code.put(Code.bprint);
		} else {
			Code.loadConst(7);
			Code.put(Code.print);
		}
	}

	public void visit(MatchedPrintNumConst matchedPrintNumConst) {
		if (matchedPrintNumConst.getExpr().struct == MyTab.charType) {
			Code.loadConst(2);
			Code.put(Code.bprint);
		} else {
			Code.loadConst(7);
			Code.put(Code.print);
		}
		Obj con = MyTab.insert(Obj.Con, "$", MyTab.intType);
		con.setLevel(0);
		con.setAdr(matchedPrintNumConst.getN2());

		Code.load(con);
		Code.loadConst(7);
		Code.put(Code.print);
	}

	public void visit(FactorNumConst factorNumConst) {
		Obj con = MyTab.insert(Obj.Con, "$", factorNumConst.struct);
		con.setLevel(0);
		con.setAdr(factorNumConst.getN1());

		Code.load(con);
	}

	public void visit(FactorPrintChar factorPrintChar) {
		Obj con = MyTab.insert(Obj.Con, "$", factorPrintChar.struct);
		con.setLevel(0);
		con.setAdr(factorPrintChar.getP1().charAt(1));
		Code.load(con);
	}

	public void visit(FactorDesignator factorDesignator) {
		if (factorDesignator.getDesignator().obj.getName().compareTo("eol") == 0) {
			Obj con = MyTab.insert(Obj.Con, "$", factorDesignator.struct);
			con.setLevel(0);
			con.setAdr('\n');

			Code.load(con);
		}
	}

	public void visit(FactorBoolConst factorBoolConst) {
		Obj con = MyTab.insert(Obj.Con, "$", factorBoolConst.struct);
		con.setLevel(0);
		con.setAdr(factorBoolConst.getBoolconst().obj.getAdr());

		Code.load(con);
	}

	public void visit(MethodTypeName methodTypeName) {
		if ("main".equalsIgnoreCase(methodTypeName.getMethName())) {
			mainPc = Code.pc;
		}
		methodTypeName.obj.setAdr(Code.pc);
		SyntaxNode methodNode = methodTypeName.getParent();

		VarCounter varCnt = new VarCounter();
		methodNode.traverseTopDown(varCnt);

		FormParamCounter fpCnt = new FormParamCounter();
		methodNode.traverseTopDown(fpCnt);
		Code.put(Code.enter);
		Code.put(fpCnt.getCount());
		Code.put(fpCnt.getCount() + varCnt.getCount());
	}

	public void visit(MethodDecl methodDecl) {
		if (methodDecl.getMethodTypeName().obj.getType() == MyTab.noType) {
			Code.put(Code.exit);
			Code.put(Code.return_);
		} else {
			Code.put(Code.trap);
			Code.put(1);
		}
	}

	public void visit(Assignment assignment) {
		if (assignment.getDesignator().getClass() == DesignatorSquare.class) {
			Obj temp = new Obj(Obj.Elem, assignment.getDesignator().obj.getName(),
					assignment.getDesignator().obj.getType(), assignment.getDesignator().obj.getAdr(),
					assignment.getDesignator().obj.getLevel());
			Code.store(temp);
		} else {
			Code.store(assignment.getDesignator().obj);
		}
	}

	public void visit(DesignatorIdent designatorIdent) {
		SyntaxNode parent = designatorIdent.getParent();

		if ((Assignment.class != parent.getClass()) && (FuncCall.class != parent.getClass())
				&& (FactorFuncCall.class != parent.getClass()) && (MatchedRead.class != parent.getClass())
				&& (designatorIdent.obj.getKind() != Obj.Type)) {
			Code.load(designatorIdent.obj);
		}
	}

	public void visit(DesignatorSquare designatorSquare) {
		SyntaxNode parent = designatorSquare.getParent();
		designatorSquare.obj = new Obj(Obj.Elem, designatorSquare.obj.getName(), designatorSquare.obj.getType(),
				designatorSquare.obj.getAdr(), designatorSquare.obj.getLevel());
		if ((Assignment.class != parent.getClass()) && (FuncCall.class != parent.getClass())
				&& (FactorFuncCall.class != parent.getClass()) && (MatchedRead.class != parent.getClass())
				&& (designatorSquare.obj.getKind() != Obj.Type) && (DesignatorDec.class != parent.getClass())
				&& (DesignatorInc.class != parent.getClass())) {
			Code.load(designatorSquare.obj);
		}
	}

	public void visit(FuncCall funcCall) {
		Obj functionObj = funcCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		Code.put(Code.call);

		Code.put2(offset);
		if (funcCall.getDesignator().obj.getType() != MyTab.noType) {
			Code.put(Code.pop);
		}
	}

	public void visit(FactorFuncCall FactorFuncCall) {
		Obj functionObj = FactorFuncCall.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		Code.put(Code.call);

		Code.put2(offset);
	}

	public void visit(MatchedReturnExpr matchedReturnExpr) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}

	public void visit(MatchedReturn matchedReturn) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}

	public void visit(ExprAddop exprAddop) {
		if (exprAddop.getAddop() instanceof Plus) {
			Code.put(Code.add);
		} else {
			Code.put(Code.sub);
		}
	}

	public void visit(TermMulop termMulop) {
		if (termMulop.getMulop() instanceof MulOperation) {
			Code.put(Code.mul);
		} else if (termMulop.getMulop() instanceof Div) {
			Code.put(Code.div);
		} else {
			Code.put(Code.rem);
		}
	}

	public void visit(CondFactRelop condFactRelop) {
		int temp;
		if (condFactRelop.getRelop() instanceof Equal) {
			temp = Code.eq;
		} else if (condFactRelop.getRelop() instanceof Nequal) {
			temp = Code.ne;
		} else if (condFactRelop.getRelop() instanceof Great) {
			temp = Code.gt;
		} else if (condFactRelop.getRelop() instanceof Gequal) {
			temp = Code.ge;
		} else if (condFactRelop.getRelop() instanceof Low) {
			temp = Code.lt;
		} else {
			temp = Code.le;
		}
		Code.putFalseJump(temp, 0);
		fixingFalse.peek().add(Code.pc - 2);
	}

	public void visit(CondFactOne condFactOne) {
		Code.put(Code.const_1);
		Code.putFalseJump(Code.eq, 0);
		fixingFalse.peek().add(Code.pc - 2);
	}

	public void visit(MatchedIfElse matchedIfElse) {
		LinkedList<Integer> temp = fixingTrue.pop();
		temp.forEach(adr -> Code.fixup(adr));
		fixingFalse.pop();
	}

	public void visit(UnmatchedIfElse unmatchedIfElse) {
		LinkedList<Integer> temp = fixingTrue.pop();
		temp.forEach(adr -> Code.fixup(adr));
		fixingFalse.pop();
	}

	public void visit(UnmatchedIf unmatchedIf) {
		LinkedList<Integer> fix = fixingFalse.pop();
		fix.forEach(adr -> Code.fixup(adr));
		fixingTrue.pop();
	}

	public void visit(ConditionOR conditionOR) {
		if ((conditionOR.getParent().getClass() == MatchedIfElse.class)
				|| (conditionOR.getParent().getClass() == UnmatchedIf.class)
				|| (conditionOR.getParent().getClass() == UnmatchedIfElse.class)) {
			LinkedList<Integer> fix = fixingTrue.peek();
			fix.forEach(adr -> Code.fixup(adr));
			fix.clear();
		}
		if (conditionOR.getParent().getClass() == ConditionOR.class) {
			Code.putJump(0);
			fixingTrue.peek().add(Code.pc - 2);
			fixingFalse.peek().forEach(addr -> Code.fixup(addr));
			fixingFalse.peek().clear();
		}
	}

	public void visit(ConditionOne conditionOne) {
		if ((conditionOne.getParent().getClass() == MatchedIfElse.class)
				|| (conditionOne.getParent().getClass() == UnmatchedIf.class)
				|| (conditionOne.getParent().getClass() == UnmatchedIfElse.class)) {
			LinkedList<Integer> fix = fixingTrue.peek();
			fix.forEach(adr -> Code.fixup(adr));
			fix.clear();
		}
		if (conditionOne.getParent().getClass() == ConditionOR.class) {
			Code.putJump(0);
			fixingTrue.peek().add(Code.pc - 2);
			fixingFalse.peek().forEach(addr -> Code.fixup(addr));
			fixingFalse.peek().clear();
		}
	}

	public void visit(DesignatorStatemenOne designatorStatemenOne) {
		SyntaxNode tempNode = designatorStatemenOne.getParent();
		if (((tempNode instanceof MatchedFor)
				&& (((MatchedFor) tempNode).getDesignatorStatementNULL() == designatorStatemenOne))
				|| ((tempNode instanceof UnmatchedFor)
						&& (((UnmatchedFor) tempNode).getDesignatorStatementNULL() == designatorStatemenOne))) {
			conditionStck.push(Code.pc);
		} else {
			Code.putJump(conditionStck.peek());
			LinkedList<Integer> temp = fixingTrue.peek();
			temp.forEach(adr -> Code.fixup(adr));
			temp.clear();
		}
	}

	public void visit(NoDesignatorStatementNull noDesignatorStatementNull) {
		SyntaxNode tempNode = noDesignatorStatementNull.getParent();
		if (((tempNode instanceof MatchedFor)
				&& (((MatchedFor) tempNode).getDesignatorStatementNULL() == noDesignatorStatementNull))
				|| ((tempNode instanceof UnmatchedFor)
						&& (((UnmatchedFor) tempNode).getDesignatorStatementNULL() == noDesignatorStatementNull))) {
			conditionStck.push(Code.pc);
		} else {
			Code.putJump(conditionStck.peek());
			LinkedList<Integer> temp = fixingTrue.peek();
			temp.forEach(adr -> Code.fixup(adr));
			temp.clear();
		}
	}

	public void visit(MatchedBreak matchedBreak) {
		Code.putJump(0);
		stackBreak.peek().add(Code.pc - 2);
	}

	public void visit(MatchedContinue matchedContinue) {
		Code.putJump(0);
		stackContinue.peek().add(Code.pc - 2);
	}

	private void forEnd() {
		stackContinue.peek().forEach(adr -> Code.fixup(adr));
		Code.putJump(Designator2Stck.peek());
		LinkedList<Integer> fix = fixingFalse.pop();
		fix.forEach(adr -> Code.fixup(adr));
		fixingTrue.pop();
		stackBreak.peek().forEach(adr -> Code.fixup(adr));
		conditionStck.pop();
		Designator2Stck.pop();
		stackContinue.pop();
		stackBreak.pop();
	}

	public void visit(MatchedFor matchedFor) {
		forEnd();
	}

	public void visit(UnmatchedFor unmatchedFor) {
		forEnd();
	}

	public void visit(ForConditionNotNULL forConditionNotNULL) {
		Code.putJump(0);
		fixingTrue.peek().add(Code.pc - 2);
		Designator2Stck.push(Code.pc);
	}

	public void visit(NoForConditionNULL noForConditionNULL) {
		Code.putJump(0);
		fixingTrue.peek().add(Code.pc - 2);
		Designator2Stck.push(Code.pc);
	}

	public void visit(MatchedRead matchedRead) {
		Obj obj = matchedRead.getDesignator().obj;
		if (obj.getType() == MyTab.charType) {
			Code.put(Code.bread);
		} else {
			Code.put(Code.read);
		}
		Code.store(obj);
	}

	public void visit(DesignatorInc designatorInc) {
		if (designatorInc.getDesignator().obj.getKind() == Obj.Elem) {
			Code.put(Code.dup2);
			Code.load(designatorInc.getDesignator().obj);
		}
		Code.load(constIncDec);
		Code.put(Code.add);
		Code.store(designatorInc.getDesignator().obj);
	}

	public void visit(DesignatorDec designatorDec) {
		if (designatorDec.getDesignator().obj.getKind() == Obj.Elem) {
			Code.put(Code.dup2);
			Code.load(designatorDec.getDesignator().obj);
		}
		Code.load(constIncDec);
		Code.put(Code.sub);
		Code.store(designatorDec.getDesignator().obj);
	}

	public void visit(ExprMinusTerm exprMinusTerm) {
		Code.put(Code.neg);
	}

	public void visit(FactorNewSquare factorNewSquare) {
		Code.put(Code.newarray);
		if (factorNewSquare.getType().struct == MyTab.charType) {
			Code.put(0);
		} else {
			Code.put(1);
		}
	}

	public void visit(EpsilonHelp epsilonHelp) {
		if ((epsilonHelp.getParent().getClass() == UnmatchedFor.class)
				|| (epsilonHelp.getParent().getClass() == MatchedFor.class)) {
			fixingFalse.push(new LinkedList<Integer>());
			fixingTrue.push(new LinkedList<Integer>());
			stackBreak.push(new LinkedList<Integer>());
			stackContinue.push(new LinkedList<Integer>());
		}
		if ((epsilonHelp.getParent().getClass() == UnmatchedIf.class)
				|| ((epsilonHelp.getParent().getClass() == UnmatchedIfElse.class)
						&& (((UnmatchedIfElse) epsilonHelp.getParent()).getEpsilonHelp() == epsilonHelp))
				|| ((epsilonHelp.getParent().getClass() == MatchedIfElse.class)
						&& (((MatchedIfElse) epsilonHelp.getParent()).getEpsilonHelp() == epsilonHelp))) {
			fixingFalse.push(new LinkedList<Integer>());
			fixingTrue.push(new LinkedList<Integer>());
		}
		if (((epsilonHelp.getParent().getClass() == UnmatchedIfElse.class)
				&& (((UnmatchedIfElse) epsilonHelp.getParent()).getEpsilonHelp1() == epsilonHelp))
				|| ((epsilonHelp.getParent().getClass() == MatchedIfElse.class)
						&& (((MatchedIfElse) epsilonHelp.getParent()).getEpsilonHelp1() == epsilonHelp))) {
			Code.putJump(0);
			fixingTrue.peek().add(Code.pc - 2);
			LinkedList<Integer> fix = fixingFalse.peek();
			fix.forEach(adr -> Code.fixup(adr));
		}

	}
}
