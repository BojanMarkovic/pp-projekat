
//----------------------------------------------------
// The following code was generated by CUP v0.10k
// Sun Feb 02 10:34:58 CET 2020
//----------------------------------------------------

package rs.ac.bg.etf.pp1;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int PRINTCHAR = 43;
  public static final int CONST = 8;
  public static final int GEQUAL = 38;
  public static final int NUMCONST = 42;
  public static final int GREAT = 31;
  public static final int LPAREN = 12;
  public static final int SEMI = 5;
  public static final int CONTINUE = 17;
  public static final int FOR = 36;
  public static final int MINUS = 34;
  public static final int RPAREN = 13;
  public static final int AND = 24;
  public static final int OR = 23;
  public static final int COMMA = 10;
  public static final int CLASS = 40;
  public static final int INC = 21;
  public static final int DIV = 28;
  public static final int PLUS = 35;
  public static final int ASSIGN = 9;
  public static final int IF = 14;
  public static final int ABSTRACT = 39;
  public static final int EOF = 0;
  public static final int RETURN = 18;
  public static final int LOW = 30;
  public static final int EQUAL = 33;
  public static final int TRUE = 44;
  public static final int NEW = 25;
  public static final int error = 1;
  public static final int PROGRAM = 2;
  public static final int MUL = 27;
  public static final int MOD = 29;
  public static final int IDENT = 46;
  public static final int BREAK = 16;
  public static final int VOID = 11;
  public static final int LBRACE = 3;
  public static final int ELSE = 15;
  public static final int LSQUARE = 7;
  public static final int LEQUAL = 37;
  public static final int POINT = 26;
  public static final int READ = 19;
  public static final int RSQUARE = 6;
  public static final int NEQUAL = 32;
  public static final int RBRACE = 4;
  public static final int EXTENDS = 41;
  public static final int DEC = 22;
  public static final int FALSE = 45;
  public static final int PRINT = 20;
}

