package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;

public class CounterVisitor extends VisitorAdaptor {
	protected int count;

	public int getCount() {
		return count;
	}

	public static class FormParamCounter extends CounterVisitor {

		public void visit(FormalParamDeclNoSquare formalParamDeclNoSquare) {
			count++;
		}

		public void visit(FormalParamDeclSquare formalParamDeclSquare) {
			count++;
		}
	}

	public static class VarCounter extends CounterVisitor {

		public void visit(WithoutSquareVarDeclElem withoutSquareVarDeclElem) {
			count++;
		}

		public void visit(SquareVarDeclElem squareVarDeclElem) {
			count++;
		}
	}

	public static class ClassCounter extends CounterVisitor {
		public void visit(AbstractClassDecl abstractClassDecl) {
			count++;
		}

		public void visit(ClassDecl classDecl) {
			count++;
		}
	}

	public static class GlobalConstCount extends CounterVisitor {
		public void visit(ConstDeclElemNumConst constDeclElemNumConst) {
			count++;
		}

		public void visit(ConstDeclElemBoolConst constDeclElemBoolConst) {
			count++;
		}

		public void visit(ConstDeclElemPrintChar constDeclElemPrintChar) {
			count++;
		}
	}

	public static class GlobalArrayCount extends CounterVisitor {
		private boolean globalno = true;

		public void visit(SquareVarDeclElem squareVarDeclElem) {
			if (globalno)
				count++;
		}

		public void visit(NoDeclarationLists noDeclarationLists) {
			globalno = false;
		}
	}

	public static class FuncCallCount extends CounterVisitor {
		private boolean funkcija = false;
		private String ime = "";
		private int StatementCount = 0;

		public FuncCallCount(String i) {
			this.ime = i;
		}

		public void visit(FuncCall funcCall) {
			if (funkcija)
				count++;
		}

		public void visit(FactorFuncCall factorFuncCall) {
			if (funkcija)
				count++;
		}

		public void visit(MethodTypeName methodTypeName) {
			if (methodTypeName.getMethName().compareToIgnoreCase(ime) == 0) {
				funkcija = true;
			}
		}

		public void visit(MethodDecl methodDecl) {

		}

		public int getStatementCount() {
			return StatementCount;
		}

		public void visit(StatementUnmatched statementUnmatched) {
			if (funkcija)
				StatementCount++;
		}

		public void visit(StatementMatched statementMatched) {
			if (funkcija)
				StatementCount++;
		}

	}
}
