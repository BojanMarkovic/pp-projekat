package rs.ac.bg.etf.pp1;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.CounterVisitor.ClassCounter;
import rs.ac.bg.etf.pp1.CounterVisitor.FuncCallCount;
import rs.ac.bg.etf.pp1.CounterVisitor.GlobalArrayCount;
import rs.ac.bg.etf.pp1.CounterVisitor.GlobalConstCount;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class SemanticAnalyze extends VisitorAdaptor {

	Type currentVarType = null;
	Obj currentMethod = null;
	Struct currentMethodType = null;
	boolean currentMethodNoArguments = false;
	boolean returnFound = false;
	boolean errorDetected = false;
	boolean inFor = false;
	boolean inMethod = false;
	boolean relOp = false;
	boolean specialFunctions = false;
	boolean globalno = true;
	int brGlobalnihPromenljivih = 0;
	int brForLoops = 0;

	class Elem {
		Obj method;
		LinkedList<Struct> params = new LinkedList<>();
		LinkedList<String> variables = new LinkedList<>();
	}

	LinkedList<Elem> allMethodsParams = new LinkedList<>();
	LinkedList<Struct> actualParams = new LinkedList<>();

	Logger log = Logger.getLogger(getClass());

	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if (line != 0)
			msg.append(" na liniji ").append(line);
		// log.error(msg.toString());
		System.err.println(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if (line != 0)
			msg.append(" na liniji ").append(line);
		log.info(msg.toString());
	}

	public void visit(ProgName progName) {
		progName.obj = MyTab.insert(Obj.Prog, progName.getProgName(), MyTab.noType);
		MyTab.openScope();
	}

	public void visit(Program program) {
		if ((MyTab.find("main") == MyTab.noObj) && (MyTab.find("Main") == MyTab.noObj)) {
			report_error("Main metoda nije definisana", null);
		}
		MyTab.chainLocalSymbols(program.getProgName().obj);
		MyTab.closeScope();

		log.info("===================================");
		log.info("Statistika:");
		ClassCounter classCnt = new ClassCounter();
		program.traverseTopDown(classCnt);
		log.info(classCnt.getCount() + " klasa");
		log.info(allMethodsParams.size() + " metoda");
		log.info(brGlobalnihPromenljivih + " globalnih promenljivih");
		GlobalConstCount constCnt = new GlobalConstCount();
		program.traverseTopDown(constCnt);
		log.info(constCnt.getCount() + " konstanti");
		GlobalArrayCount arrayCnt = new GlobalArrayCount();
		program.traverseTopDown(arrayCnt);
		log.info(arrayCnt.getCount() + " globalnih nizova");
		for (int i = 0; i < allMethodsParams.size(); i++) {
			if (allMethodsParams.get(i).method.getName().compareToIgnoreCase("main") == 0) {
				Obj temp = allMethodsParams.get(i).method;
				int num = temp.getLocalSymbols().size() - temp.getLevel();
				log.info(num + " lokalnih promenljivih u " + temp.getName());
				FuncCallCount funcCnt = new FuncCallCount(temp.getName());
				program.traverseBottomUp(funcCnt);
				log.info(funcCnt.getCount() + " poziva funkcija");
				log.info(funcCnt.getStatementCount() + " statementa");
				break;
			}
		}
	}

	public boolean proveriPromenljivuFunkcije(String name) {
		if (!allMethodsParams.isEmpty()) {
			if (allMethodsParams.getLast().variables.contains(name)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	public boolean proveriFormalniParametarFunkcije(String name) {
		if (!allMethodsParams.isEmpty()) {
			for (int i = 0; i < currentMethod.getLevel(); i++) {
				if (allMethodsParams.getLast().variables.get(i).compareTo(name) == 0) {
					return true;
				}
			}
		} else {
			return true;
		}
		return false;
	}

	public void visit(WithoutSquareVarDeclElem withoutSquareVarDeclElem) {
		if ((MyTab.find(withoutSquareVarDeclElem.getVarName()) != MyTab.noObj)
				&& (proveriPromenljivuFunkcije(withoutSquareVarDeclElem.getVarName()))) {
			report_error("Ime je vec definisano " + withoutSquareVarDeclElem.getVarName(), withoutSquareVarDeclElem);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip promenljive" + withoutSquareVarDeclElem.getVarName(),
						withoutSquareVarDeclElem);
				return;
			}
			Obj tempObj = MyTab.insert(Obj.Var, withoutSquareVarDeclElem.getVarName(), currentVarType.struct);
			if (!allMethodsParams.isEmpty())
				allMethodsParams.getLast().variables.add(withoutSquareVarDeclElem.getVarName());
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(tempObj);
			report_info("Deklarisana promenljiva " + withoutSquareVarDeclElem.getVarName() + ", objekat: "
					+ temp.getOutput(), withoutSquareVarDeclElem);
			if (globalno) {
				brGlobalnihPromenljivih++;
				tempObj.setLevel(tempObj.getLevel() - 1);
			}
		}
	}

	public void visit(SquareVarDeclElem squareVarDeclElem) {
		if ((MyTab.find(squareVarDeclElem.getVarName()) != MyTab.noObj)
				&& (proveriPromenljivuFunkcije(squareVarDeclElem.getVarName()))) {
			report_error("Ime je vec definisano " + squareVarDeclElem.getVarName(), squareVarDeclElem);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip promenljive" + squareVarDeclElem.getVarName(), squareVarDeclElem);
				return;
			}
			Obj tempObj = MyTab.insert(Obj.Var, squareVarDeclElem.getVarName(),
					new Struct(Struct.Array, currentVarType.struct));
			if (!allMethodsParams.isEmpty())
				allMethodsParams.getLast().variables.add(squareVarDeclElem.getVarName());

			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(tempObj);
			report_info("Deklarisana promenljiva " + squareVarDeclElem.getVarName() + ", objekat: " + temp.getOutput(),
					squareVarDeclElem);
			if (globalno) {
				brGlobalnihPromenljivih++;
				tempObj.setLevel(tempObj.getLevel() - 1);
			}
		}
	}

	public void visit(NoMethodDeclList noMethodDeclList) {
		globalno = false;
		if (brGlobalnihPromenljivih > 256)
			report_error("Nije dozvoljen ovoliki broj Globalnih promenljivih", null);

	}

	public void visit(TrueBool trueBool) {
		trueBool.obj = new Obj(Obj.Con, "true", MyTab.boolType);
		trueBool.obj.setAdr(1);
	}

	public void visit(FalseBool falseBool) {
		falseBool.obj = new Obj(Obj.Con, "false", MyTab.boolType);
		falseBool.obj.setAdr(0);
	}

	public void visit(ConstDeclElemNumConst constDeclElemNumConst) {
		if (MyTab.find(constDeclElemNumConst.getConstName()) != MyTab.noObj) {
			report_error("Ime je vec definisano " + constDeclElemNumConst.getConstName(), constDeclElemNumConst);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip konstante" + constDeclElemNumConst.getConstName(),
						constDeclElemNumConst);
				return;
			}
			if (currentVarType.struct != MyTab.intType) {
				report_error("Nije validan tip konstante i dodeljene vrednosti" + constDeclElemNumConst.getConstName(),
						constDeclElemNumConst);
				return;
			}
			Obj con = MyTab.insert(Obj.Con, constDeclElemNumConst.getConstName(), MyTab.intType);
			con.setAdr(constDeclElemNumConst.getN1());
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(constDeclElemNumConst.getConstName()));
			report_info(
					"Deklarisana konstanta " + constDeclElemNumConst.getConstName() + ", objekat: " + temp.getOutput(),
					constDeclElemNumConst);
		}
	}

	public void visit(ConstDeclElemBoolConst constDeclElemBoolConst) {
		if (MyTab.find(constDeclElemBoolConst.getConstName()) != MyTab.noObj) {
			report_error("Ime je vec definisano " + constDeclElemBoolConst.getConstName(), constDeclElemBoolConst);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip konstante" + constDeclElemBoolConst.getConstName(),
						constDeclElemBoolConst);
				return;
			}
			if (currentVarType.struct != MyTab.boolType) {
				report_error("Nije validan tip konstante i dodeljene vrednosti" + constDeclElemBoolConst.getConstName(),
						constDeclElemBoolConst);
				return;
			}
			Obj con = MyTab.insert(Obj.Con, constDeclElemBoolConst.getConstName(), MyTab.boolType);
			con.setAdr(constDeclElemBoolConst.getBoolconst().obj.getAdr());
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(constDeclElemBoolConst.getConstName()));
			report_info(
					"Deklarisana konstanta " + constDeclElemBoolConst.getConstName() + ", objekat: " + temp.getOutput(),
					constDeclElemBoolConst);
		}
	}

	public void visit(ConstDeclElemPrintChar constDeclElemPrintChar) {
		if (MyTab.find(constDeclElemPrintChar.getConstName()) != MyTab.noObj) {
			report_error("Ime je vec definisano " + constDeclElemPrintChar.getConstName(), constDeclElemPrintChar);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip konstante" + constDeclElemPrintChar.getConstName(),
						constDeclElemPrintChar);
				return;
			}
			if (currentVarType.struct != MyTab.charType) {
				report_error("Nije validan tip konstante i dodeljene vrednosti" + constDeclElemPrintChar.getConstName(),
						constDeclElemPrintChar);
				return;
			}
			Obj con = MyTab.insert(Obj.Con, constDeclElemPrintChar.getConstName(), MyTab.charType);
			con.setAdr(constDeclElemPrintChar.getCharval().charAt(1));
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(constDeclElemPrintChar.getConstName()));
			report_info(
					"Deklarisana konstanta " + constDeclElemPrintChar.getConstName() + ", objekat: " + temp.getOutput(),
					constDeclElemPrintChar);
		}
	}

	public void visit(Type type) {
		currentVarType = type;
		Obj typeNode = MyTab.find(type.getTypeName());
		if (typeNode == MyTab.noObj) {
			report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", type);
			type.struct = MyTab.noType;
		} else {
			if (Obj.Type == typeNode.getKind()) {
				type.struct = typeNode.getType();
			} else {
				report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
				type.struct = MyTab.noType;
			}
		}
	}

	public void visit(MethodTypeName methodTypeName) {
		if (MyTab.find(methodTypeName.getMethName()) != MyTab.noObj) {
			report_error("Ime je vec definisano " + methodTypeName.getMethName(), methodTypeName);
		} else {
			currentMethod = MyTab.insert(Obj.Meth, methodTypeName.getMethName(), currentMethodType);
			currentMethod.setLevel(0);
			Elem temp = new Elem();
			temp.method = currentMethod;
			allMethodsParams.add(temp);
			methodTypeName.obj = currentMethod;
			MyTab.openScope();
			report_info("Obradjuje se funkcija " + methodTypeName.getMethName(), methodTypeName);
		}
	}

	public void visit(FormParsOne formParsOne) {
		if (!allMethodsParams.isEmpty())
			allMethodsParams.getLast().params.addFirst(formParsOne.getFormalParamDecl().struct);
	}

	public void visit(FormParsComma formParsComma) {
		if (!allMethodsParams.isEmpty())
			allMethodsParams.getLast().params.add(formParsComma.getFormalParamDecl().struct);
	}

	public void visit(FormalParamDeclNoSquare formalParamDeclNoSquare) {
		formalParamDeclNoSquare.struct = formalParamDeclNoSquare.getType().struct;
		if ((MyTab.find(formalParamDeclNoSquare.getI2()) != MyTab.noObj)
				&& (proveriPromenljivuFunkcije(formalParamDeclNoSquare.getI2()))) {
			report_error("Ime je vec definisano " + formalParamDeclNoSquare.getI2(), formalParamDeclNoSquare);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip promenljive" + formalParamDeclNoSquare.getI2(),
						formalParamDeclNoSquare);
				return;
			}
			MyTab.insert(Obj.Var, formalParamDeclNoSquare.getI2(), currentVarType.struct);
			if (!allMethodsParams.isEmpty())
				allMethodsParams.getLast().variables.add(formalParamDeclNoSquare.getI2());
			if (currentMethod != null)
				currentMethod.setLevel(currentMethod.getLevel() + 1);
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(formalParamDeclNoSquare.getI2()));
			report_info("Deklarisana promenljiva " + formalParamDeclNoSquare.getI2() + ", objekat: " + temp.getOutput(),
					formalParamDeclNoSquare);
			if (globalno) {
				brGlobalnihPromenljivih++;
			}
		}
	}

	public void visit(FormalParamDeclSquare formalParamDeclSquare) {
		formalParamDeclSquare.struct = formalParamDeclSquare.getType().struct;
		if ((MyTab.find(formalParamDeclSquare.getI2()) != MyTab.noObj)
				&& (proveriPromenljivuFunkcije(formalParamDeclSquare.getI2()))) {
			report_error("Ime je vec definisano " + formalParamDeclSquare.getI2(), formalParamDeclSquare);
		} else {
			if (currentVarType == null) {
				report_error("Nije pronadjen tip promenljive" + formalParamDeclSquare.getI2(), formalParamDeclSquare);
				return;
			}
			MyTab.insert(Obj.Var, formalParamDeclSquare.getI2(), new Struct(Struct.Array, currentVarType.struct));
			if (!allMethodsParams.isEmpty())
				allMethodsParams.getLast().variables.add(formalParamDeclSquare.getI2());
			if (currentMethod != null)
				currentMethod.setLevel(currentMethod.getLevel() + 1);
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(formalParamDeclSquare.getI2()));
			report_info("Deklarisana promenljiva " + formalParamDeclSquare.getI2() + ", objekat: " + temp.getOutput(),
					formalParamDeclSquare);
			if (globalno) {
				brGlobalnihPromenljivih++;
			}
		}
	}

	public void visit(MethodTypeType methodTypeType) {
		inMethod = true;
		currentMethodType = methodTypeType.getType().struct;
	}

	public void visit(MethodTypeVoid methodTypeVoid) {
		inMethod = true;
		currentMethodType = MyTab.noType;
	}

	public void visit(MethodDecl methodDecl) {
		if (!returnFound && currentMethodType != MyTab.noType) {
			report_error("Semanticka greska " + ": funkcija " + currentMethod.getName() + " nema return iskaz!",
					methodDecl);
		}
		MyTab.chainLocalSymbols(currentMethod);
		MyTab.closeScope();
		if ((currentMethod.getName().compareTo("main") == 0) || (currentMethod.getName().compareTo("Main") == 0)) {
			if (currentMethod.getType() != MyTab.noType) {
				report_error("Greska " + ": funkcija " + currentMethod.getName() + " mora imati void povratni tip!",
						methodDecl);
			}
			if (currentMethodNoArguments == false) {
				report_error("Greska " + ": funkcija " + currentMethod.getName() + " ne sme imati argumente!",
						methodDecl);
			}
		}
		if (Tab.find(currentMethod.getName()).getLevel() > 256)
			report_error(
					"Nije dozvoljen ovoliki broj lokalnih promenljivih u funkciji " + currentMethod.getName() + " ",
					methodDecl);
		currentMethod = null;
		currentMethodType = null;
		currentMethodNoArguments = false;
	}

	public void visit(NoFormParsNULL noFormParsNULL) {
		currentMethodNoArguments = true;
	}

	public void visit(VarDecl varDecl) {
		currentVarType = null;
	}

	public void detektovanjeSimbola(SyntaxNode syNode, String name) {
		MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
		Obj tempIdent = MyTab.find(name);
		temp.visitObjNode(tempIdent);
		switch (tempIdent.getKind()) {
		case Obj.Con:
			report_info("Koriscenje konstante " + name + ", objekat: " + temp.getOutput(), syNode);
			break;
		case Obj.Var:
			if (currentMethod != null) {
				if (proveriFormalniParametarFunkcije(name)) {
					report_info("Koriscenje formalnog parametra " + name + ", objekat: " + temp.getOutput(), syNode);
				} else {
					if (tempIdent.getType().getKind() == Struct.Array) {
						report_info("Koriscenje niza " + name + ", objekat: " + temp.getOutput(), syNode);
					} else {
						report_info("Koriscenje promenljive " + name + ", objekat: " + temp.getOutput(), syNode);
					}
				}
			} else {
				if (tempIdent.getType().getKind() == Struct.Array) {
					report_info("Koriscenje niza " + name + ", objekat: " + temp.getOutput(), syNode);
				} else {
					report_info("Koriscenje promenljive " + name + ", objekat: " + temp.getOutput(), syNode);
				}
			}
			break;
		default:
			break;
		}
	}

	public void visit(DesignatorIdent designatorIdent) {
		Obj obj = MyTab.find(designatorIdent.getIdentName());
		if (obj == MyTab.noObj) {
			report_error("Greska " + " : ime " + designatorIdent.getIdentName() + " nije deklarisano! ",
					designatorIdent);
		} else {
			detektovanjeSimbola(designatorIdent, designatorIdent.getIdentName());
		}
		designatorIdent.obj = obj;
	}

	public Elem pronadjiFunkciju(String name) {
		for (int i = 0; i < allMethodsParams.size(); i++) {
			if (allMethodsParams.get(i).method.getName().compareTo(name) == 0) {
				return allMethodsParams.get(i);
			}
		}
		return null;
	}

	public void visit(FuncCall funcCall) {
		Obj func = funcCall.getDesignator().obj;
		if (Obj.Meth == func.getKind()) {
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(func.getName()));
			report_info("Pronadjen poziv funkcije " + func.getName() + ", objekat: " + temp.getOutput(), funcCall);
			funcCall.struct = func.getType();
		} else {
			report_error("Greska " + " : ime " + func.getName() + " nije funkcija!", funcCall);
			funcCall.struct = MyTab.noType;
		}

		if ((func.getName().compareTo("chr") == 0) || (func.getName().compareTo("ord") == 0)
				|| (func.getName().compareTo("len") == 0)) {
			specialFunctions = true;
		}

		if (!specialFunctions) {
			Elem temp = pronadjiFunkciju(func.getName());
			if (temp == null) {
				report_error("Greska " + " funkcija nije definisana", funcCall);
			} else {
				if (temp.params.size() != actualParams.size()) {
					report_error("Greska " + " : u funkciji " + func.getName()
							+ " formalni i stvarni argumenti nisu kompatibilni", funcCall);
				} else {
					boolean greska = false;
					for (int i = 0; i < temp.params.size(); i++) {
						if (!temp.params.get(i).compatibleWith(actualParams.get(i))) {
							greska = true;
						}
					}
					if (greska) {
						report_error("Greska " + " : u funkciji " + func.getName()
								+ " formalni i stvarni argumenti nisu kompatibilni", funcCall);
					}
				}
			}
		}

		if (specialFunctions) {
			if (func.getName().compareTo("chr") == 0) {
				if (!funcCall.getActualPars().struct.compatibleWith(MyTab.intType)) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar", funcCall);
				}
			}
			if (func.getName().compareTo("ord") == 0) {
				if (!funcCall.getActualPars().struct.compatibleWith(MyTab.charType)) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar", funcCall);
				}
			}
			if (func.getName().compareTo("len") == 0) {
				if (funcCall.getActualPars().struct.getKind() != Struct.Array) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar", funcCall);
				}
			}
		}

		actualParams = new LinkedList<>();
		specialFunctions = false;
	}

	public void visit(ActualParamListExpr actualParamListExpr) {
		actualParamListExpr.struct = actualParamListExpr.getExpr().struct;
		actualParams.addFirst(actualParamListExpr.struct);
	}

	public void visit(ActualParamListComma actualParamListComma) {
		actualParams.add(actualParamListComma.getExpr().struct);
	}

	public void visit(ActualParsOne actualParsOne) {
		actualParsOne.struct = actualParsOne.getActualParamList().struct;
	}

	public void visit(FactorFuncCall factorFuncCall) {
		Obj func = factorFuncCall.getDesignator().obj;
		if (Obj.Meth == func.getKind()) {
			MyDumpSymbolTableVisitor temp = new MyDumpSymbolTableVisitor();
			temp.visitObjNode(MyTab.find(func.getName()));
			report_info("Pronadjen poziv funkcije " + func.getName() + ", objekat: " + temp.getOutput(),
					factorFuncCall);
			factorFuncCall.struct = func.getType();
		} else {
			report_error("Greska " + " : ime " + func.getName() + " nije funkcija!", null);
			factorFuncCall.struct = MyTab.noType;
		}

		if ((func.getName().compareTo("chr") == 0) || (func.getName().compareTo("ord") == 0)
				|| (func.getName().compareTo("len") == 0)) {
			specialFunctions = true;
		}

		if (!specialFunctions) {
			Elem temp = pronadjiFunkciju(func.getName());
			if (temp == null) {
				report_error("Greska " + " funkcija nije definisana", null);
			} else {
				if (temp.params.size() != actualParams.size()) {
					report_error("Greska " + " : u funkciji " + func.getName()
							+ " formalni i stvarni argumenti nisu kompatibilni", null);
				} else {
					boolean greska = false;
					for (int i = 0; i < temp.params.size(); i++) {
						if (temp.params.get(i).getKind() != actualParams.get(i).getKind()) {
							greska = true;
						}
					}
					if (greska) {
						report_error("Greska " + " : u funkciji " + func.getName()
								+ " formalni i stvarni argumenti nisu kompatibilni", null);
					}
				}
			}
		}

		if (specialFunctions) {
			if (func.getName().compareTo("chr") == 0) {
				if (!factorFuncCall.getActualPars().struct.compatibleWith(MyTab.intType)) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar",
							factorFuncCall);
				}
			}
			if (func.getName().compareTo("ord") == 0) {
				if (!factorFuncCall.getActualPars().struct.compatibleWith(MyTab.charType)) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar",
							factorFuncCall);
				}
			}
			if (func.getName().compareTo("len") == 0) {
				if (factorFuncCall.getActualPars().struct.getKind() != Struct.Array) {
					report_error("Greska " + " : ime " + func.getName() + " mora imati INT kao parametar",
							factorFuncCall);
				}
			}
		}

		actualParams = new LinkedList<>();
		specialFunctions = false;
	}

	public void visit(TermFactor termFactor) {
		termFactor.struct = termFactor.getFactor().struct;
	}

	public void visit(TermMulop termMulop) {
		Struct te = termMulop.getFactor().struct;
		Struct t = termMulop.getTerm().struct;
		if ((te != MyTab.intType) || (t != MyTab.intType)) {
			report_error("Greska " + " : izraz mora biti tipa int", termMulop);
		}
		if ((te.compatibleWith(t)) && (te == MyTab.intType)) {
			termMulop.struct = te;
		} else {
			report_error("Greska " + " : nekompatibilni tipovi u izrazu za mnozenje.", termMulop);
			termMulop.struct = MyTab.noType;
		}
	}

	public void visit(ExprMinusTerm exprMinusTerm) {
		if (exprMinusTerm.getTerm().struct != MyTab.intType) {
			report_error("Greska " + " : izraz posle minusa mora biti int tipa", exprMinusTerm);
		}
		exprMinusTerm.struct = exprMinusTerm.getTerm().struct;
	}

	public void visit(ExprTerm exprTerm) {
		exprTerm.struct = exprTerm.getTerm().struct;
	}

	public void visit(ExprAddop exprAddop) {
		Struct te = exprAddop.getExpr().struct;
		Struct t = exprAddop.getTerm().struct;
		if ((te != MyTab.intType) || (t != MyTab.intType)) {
			report_error("Greska " + " : izraz mora biti tipa int", exprAddop);
		}
		if (te.compatibleWith(t)) {
			exprAddop.struct = te;
		} else {
			report_error("Greska " + " : nekompatibilni tipovi u izrazu za sabiranje.", exprAddop);
			exprAddop.struct = MyTab.noType;
		}
	}

	public void visit(FactorNumConst factorNumConst) {
		factorNumConst.struct = MyTab.intType;
	}

	public void visit(FactorPrintChar factorPrintChar) {
		factorPrintChar.struct = MyTab.charType;
	}

	public void visit(FactorBoolConst factorBoolConst) {
		factorBoolConst.struct = MyTab.boolType;
	}

	public void visit(FactorParen factorParen) {
		factorParen.struct = factorParen.getExpr().struct;
	}

	public void visit(FactorDesignator factorDesignator) {
		factorDesignator.struct = factorDesignator.getDesignator().obj.getType();
	}

	public void visit(MatchedReturnExpr matchedReturnExpr) {
		if (inMethod != true) {
			report_error("Greska " + " : " + "return naredba mora da se nalazi u funkciji ", matchedReturnExpr);
		}
		returnFound = true;
		if (!currentMethodType.equals(matchedReturnExpr.getExpr().struct)) {
			report_error(
					"Greska " + " : " + "tip izraza u return naredbi ne slaze se sa tipom povratne vrednosti funkcije "
							+ currentMethod.getName(),
					matchedReturnExpr);
		}
	}

	public void visit(MatchedReturn matchedReturn) {
		if (inMethod != true) {
			report_error("Greska " + " : " + "return naredba mora da se nalazi u funkciji ", matchedReturn);
		}
		if (currentMethodType != MyTab.noType) {
			report_error("Greska " + " : " + "ova funkcija nije tipa void " + currentMethod.getName(), matchedReturn);
		}
	}

	public void visit(Assignment assignment) {
		if (assignment.getDesignator().obj.getKind() != Obj.Var) {
			report_error("Greska " + " : " + "nije moguce dodeliti vrednost ovoj promenljivoj! ", assignment);
		}
		if (!assignment.getExpr().struct.compatibleWith(assignment.getDesignator().obj.getType()))
			report_error("Greska " + " : " + "nekompatibilni tipovi u dodeli vrednosti! ", assignment);
	}

	public void visit(DesignatorInc designatorInc) {
		if (designatorInc.getDesignator().obj.getKind() != Obj.Var) {
			report_error("Greska " + " : " + "nije moguce izvrsiti operaciju ++ nad ovom promenljivom", designatorInc);
		}
		if ((designatorInc.getDesignator().obj.getType() != MyTab.intType)) {
			report_error("Greska " + " : " + "nije moguce izvrsiti operaciju ++ nad promenljivom ovog tipa",
					designatorInc);
		}
	}

	public void visit(DesignatorDec designatorDec) {
		if (designatorDec.getDesignator().obj.getKind() != Obj.Var) {
			report_error("Greska " + " : " + "nije moguce izvrsiti operaciju -- nad ovom promenljivom", designatorDec);
		}
		if ((designatorDec.getDesignator().obj.getType() != MyTab.intType)) {
			report_error("Greska " + " : " + "nije moguce izvrsiti operaciju -- nad promenljivom ovog tipa",
					designatorDec);
		}
	}

	public void visit(ForConditionNotNULL forConditionNotNULL) {
		inFor = true;
		brForLoops++;
	}

	public void visit(NoForConditionNULL noForConditionNULL) {
		inFor = true;
		brForLoops++;
	}

	public void visit(MatchedBreak matchedBreak) {
		if (inFor != true) {
			report_error("Greska " + " : break ne sme da se nalazi van for petlje", matchedBreak);
		}
	}

	public void visit(MatchedContinue matchedContinue) {
		if (inFor != true) {
			report_error("Greska " + " : continue ne sme da se nalazi van for petlje", matchedContinue);
		}
	}

	public void visit(MatchedFor matchedFor) {
		brForLoops--;
		if (brForLoops == 0)
			inFor = false;
		report_info("For petlja", matchedFor);
	}

	public void visit(UnmatchedFor unmatchedFor) {
		brForLoops--;
		if (brForLoops == 0)
			inFor = false;
		report_info("For petlja", unmatchedFor);
	}

	public void visit(MatchedRead matchedRead) {
		if (matchedRead.getDesignator().obj.getKind() != Obj.Var) {
			report_error("Greska " + " : nije moguce koristiti ovaj identifikator u read funkciji", matchedRead);
			return;
		}
		if ((matchedRead.getDesignator().obj.getType() != MyTab.intType)
				&& (matchedRead.getDesignator().obj.getType() != MyTab.charType)
				&& (matchedRead.getDesignator().obj.getType() != MyTab.boolType)) {
			report_error("Greska " + " : nije moguce koristiti promenljivu ovog tipa u read funkciji", matchedRead);
		}
	}

	public void visit(MatchedPrint matchedPrint) {
		if ((matchedPrint.getExpr().struct != MyTab.intType) && (matchedPrint.getExpr().struct != MyTab.charType)
				&& (matchedPrint.getExpr().struct != MyTab.boolType)) {
			report_error("Greska " + " : nije moguce koristiti promenljivu ovog tipa u print funkciji", matchedPrint);
		}
	}

	public void visit(MatchedPrintNumConst matchedPrintNumConst) {
		if ((matchedPrintNumConst.getExpr().struct != MyTab.intType)
				&& (matchedPrintNumConst.getExpr().struct != MyTab.charType)
				&& (matchedPrintNumConst.getExpr().struct != MyTab.boolType)) {
			report_error("Greska " + " : nije moguce koristiti promenljivu ovog tipa u print funkciji",
					matchedPrintNumConst);
		}
	}

	public void visit(CondFactRelop condFactRelop) {
		if (!condFactRelop.getExpr().struct.compatibleWith(condFactRelop.getExpr1().struct)) {
			report_error("Greska " + " : los tip izraza", condFactRelop);
		}
		if (condFactRelop.getExpr().struct.getKind() == Struct.Array) {
			if (relOp != true) {
				report_error("Greska " + " : sa nizovima je moguce samo koristiti == i !=", condFactRelop);
			}
		}
		relOp = false;
		condFactRelop.struct = MyTab.boolType;
	}

	public void visit(CondFactOne condFactOne) {
		condFactOne.struct = MyTab.boolType;
	}

	public void visit(CondTermOne condTermOne) {
		condTermOne.struct = condTermOne.getCondFact().struct;
	}

	public void visit(CondTermAnd condTermAnd) {
		if ((condTermAnd.getCondTerm().struct.getKind() != condTermAnd.getCondFact().struct.getKind())
				|| (condTermAnd.getCondTerm().struct != MyTab.boolType)) {
			report_error("Greska " + " : los tip izraza za uslov", condTermAnd);
		}
		condTermAnd.struct = condTermAnd.getCondTerm().struct;
	}

	public void visit(ConditionOne conditionOne) {
		if (conditionOne.getCondTerm().struct != MyTab.boolType) {
			report_error("Greska " + " : los tip izraza za uslov", conditionOne);
		}
		conditionOne.struct = conditionOne.getCondTerm().struct;
	}

	public void visit(ConditionOR conditionOR) {
		if ((conditionOR.getCondTerm().struct.getKind() != conditionOR.getCondition().struct.getKind())
				|| (conditionOR.getCondition().struct != MyTab.boolType)) {
			report_error("Greska " + " : los tip izraza za uslov", conditionOR);
		}
		conditionOR.struct = conditionOR.getCondition().struct;
	}

	public void visit(Equal equal) {
		relOp = true;
	}

	public void visit(Nequal nequal) {
		relOp = true;
	}

	public void visit(FactorNewSquare factorNewSquare) {
		if (factorNewSquare.getExpr().struct != MyTab.intType) {
			report_error("Greska " + " : nedozvoljen tip izraza", factorNewSquare);
		}
		factorNewSquare.struct = new Struct(Struct.Array, factorNewSquare.getType().struct);
	}

	public void visit(DesignatorSquare designatorSquare) {
		if ((designatorSquare.getExpr().struct != MyTab.intType)
				|| (designatorSquare.getDesignator().obj.getType().getKind() != Struct.Array)) {
			report_error("Greska " + " : nedozvoljen tip izraza", designatorSquare);
		}
		Obj temp = designatorSquare.getDesignator().obj;
		designatorSquare.obj = new Obj(temp.getKind(), temp.getName(), temp.getType().getElemType());
		MyDumpSymbolTableVisitor tempDump = new MyDumpSymbolTableVisitor();
		tempDump.visitObjNode(MyTab.find(temp.getName()));
		report_info("Pristupljeno nizu " + designatorSquare.getDesignator().obj.getName() + ", objekat: "
				+ tempDump.getOutput(), designatorSquare);
	}

	public boolean passed() {
		return !errorDetected;
	}

	public void visit(FactorNew factorNew) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}

	public void visit(DeclarationListsAbstractClass declarationListsAbstractClass) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}

	public void visit(DeclarationListsClass declarationListsClass) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}

	public void visit(AbstractClassDecl abstractClassDecl) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}

	public void visit(ClassDecl classDecl) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}

	public void visit(DesignatorPoint designatorPoint) {
		report_error("C nivo nije podrzan u semantickoj analizi! Prekidamo program!", null);
		System.exit(-1);
	}
}
