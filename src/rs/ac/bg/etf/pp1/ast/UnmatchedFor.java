// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class UnmatchedFor extends Unmatched {

    private EpsilonHelp EpsilonHelp;
    private DesignatorStatementNULL DesignatorStatementNULL;
    private ForConditionNULL ForConditionNULL;
    private DesignatorStatementNULL DesignatorStatementNULL1;
    private Unmatched Unmatched;

    public UnmatchedFor (EpsilonHelp EpsilonHelp, DesignatorStatementNULL DesignatorStatementNULL, ForConditionNULL ForConditionNULL, DesignatorStatementNULL DesignatorStatementNULL1, Unmatched Unmatched) {
        this.EpsilonHelp=EpsilonHelp;
        if(EpsilonHelp!=null) EpsilonHelp.setParent(this);
        this.DesignatorStatementNULL=DesignatorStatementNULL;
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.setParent(this);
        this.ForConditionNULL=ForConditionNULL;
        if(ForConditionNULL!=null) ForConditionNULL.setParent(this);
        this.DesignatorStatementNULL1=DesignatorStatementNULL1;
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.setParent(this);
        this.Unmatched=Unmatched;
        if(Unmatched!=null) Unmatched.setParent(this);
    }

    public EpsilonHelp getEpsilonHelp() {
        return EpsilonHelp;
    }

    public void setEpsilonHelp(EpsilonHelp EpsilonHelp) {
        this.EpsilonHelp=EpsilonHelp;
    }

    public DesignatorStatementNULL getDesignatorStatementNULL() {
        return DesignatorStatementNULL;
    }

    public void setDesignatorStatementNULL(DesignatorStatementNULL DesignatorStatementNULL) {
        this.DesignatorStatementNULL=DesignatorStatementNULL;
    }

    public ForConditionNULL getForConditionNULL() {
        return ForConditionNULL;
    }

    public void setForConditionNULL(ForConditionNULL ForConditionNULL) {
        this.ForConditionNULL=ForConditionNULL;
    }

    public DesignatorStatementNULL getDesignatorStatementNULL1() {
        return DesignatorStatementNULL1;
    }

    public void setDesignatorStatementNULL1(DesignatorStatementNULL DesignatorStatementNULL1) {
        this.DesignatorStatementNULL1=DesignatorStatementNULL1;
    }

    public Unmatched getUnmatched() {
        return Unmatched;
    }

    public void setUnmatched(Unmatched Unmatched) {
        this.Unmatched=Unmatched;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.accept(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.accept(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.accept(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.accept(visitor);
        if(Unmatched!=null) Unmatched.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EpsilonHelp!=null) EpsilonHelp.traverseTopDown(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.traverseTopDown(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.traverseTopDown(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.traverseTopDown(visitor);
        if(Unmatched!=null) Unmatched.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.traverseBottomUp(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.traverseBottomUp(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.traverseBottomUp(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.traverseBottomUp(visitor);
        if(Unmatched!=null) Unmatched.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("UnmatchedFor(\n");

        if(EpsilonHelp!=null)
            buffer.append(EpsilonHelp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementNULL!=null)
            buffer.append(DesignatorStatementNULL.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForConditionNULL!=null)
            buffer.append(ForConditionNULL.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementNULL1!=null)
            buffer.append(DesignatorStatementNULL1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Unmatched!=null)
            buffer.append(Unmatched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [UnmatchedFor]");
        return buffer.toString();
    }
}
