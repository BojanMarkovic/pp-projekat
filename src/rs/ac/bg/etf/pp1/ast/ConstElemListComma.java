// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class ConstElemListComma extends ConstElemList {

    private ConstDeclElem ConstDeclElem;
    private ConstElemList ConstElemList;

    public ConstElemListComma (ConstDeclElem ConstDeclElem, ConstElemList ConstElemList) {
        this.ConstDeclElem=ConstDeclElem;
        if(ConstDeclElem!=null) ConstDeclElem.setParent(this);
        this.ConstElemList=ConstElemList;
        if(ConstElemList!=null) ConstElemList.setParent(this);
    }

    public ConstDeclElem getConstDeclElem() {
        return ConstDeclElem;
    }

    public void setConstDeclElem(ConstDeclElem ConstDeclElem) {
        this.ConstDeclElem=ConstDeclElem;
    }

    public ConstElemList getConstElemList() {
        return ConstElemList;
    }

    public void setConstElemList(ConstElemList ConstElemList) {
        this.ConstElemList=ConstElemList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ConstDeclElem!=null) ConstDeclElem.accept(visitor);
        if(ConstElemList!=null) ConstElemList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ConstDeclElem!=null) ConstDeclElem.traverseTopDown(visitor);
        if(ConstElemList!=null) ConstElemList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ConstDeclElem!=null) ConstDeclElem.traverseBottomUp(visitor);
        if(ConstElemList!=null) ConstElemList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstElemListComma(\n");

        if(ConstDeclElem!=null)
            buffer.append(ConstDeclElem.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstElemList!=null)
            buffer.append(ConstElemList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstElemListComma]");
        return buffer.toString();
    }
}
