// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(Unmatched Unmatched) { }
    public void visit(Mulop Mulop) { }
    public void visit(VarDeclElem VarDeclElem) { }
    public void visit(Matched Matched) { }
    public void visit(Relop Relop) { }
    public void visit(MethodType MethodType) { }
    public void visit(FormalParamDecl FormalParamDecl) { }
    public void visit(StatementList StatementList) { }
    public void visit(Addop Addop) { }
    public void visit(Factor Factor) { }
    public void visit(CondTerm CondTerm) { }
    public void visit(ConstElemList ConstElemList) { }
    public void visit(AbstractMethodDecl AbstractMethodDecl) { }
    public void visit(Designator Designator) { }
    public void visit(Term Term) { }
    public void visit(AbstractClassMethodsList AbstractClassMethodsList) { }
    public void visit(Condition Condition) { }
    public void visit(AbstractClassMethods AbstractClassMethods) { }
    public void visit(VarElemList VarElemList) { }
    public void visit(ActualParamList ActualParamList) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(Expr Expr) { }
    public void visit(ForConditionNULL ForConditionNULL) { }
    public void visit(MethodDeclNull MethodDeclNull) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(FormParsNULL FormParsNULL) { }
    public void visit(ActualPars ActualPars) { }
    public void visit(Boolconst Boolconst) { }
    public void visit(DesignatorStatementNULL DesignatorStatementNULL) { }
    public void visit(Statement Statement) { }
    public void visit(DeclarationLists DeclarationLists) { }
    public void visit(ExtendsNull ExtendsNull) { }
    public void visit(CondFact CondFact) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(ConstDeclElem ConstDeclElem) { }
    public void visit(FormPars FormPars) { }
    public void visit(AbstractMethodDeclError AbstractMethodDeclError) { visit(); }
    public void visit(AbstractMethodDeclOne AbstractMethodDeclOne) { visit(); }
    public void visit(NoAbstractClassMethodsList NoAbstractClassMethodsList) { visit(); }
    public void visit(AbstractClassMethodsListAbstract AbstractClassMethodsListAbstract) { visit(); }
    public void visit(AbstractClassMethodsListMethod AbstractClassMethodsListMethod) { visit(); }
    public void visit(NoAbstractClassMethods NoAbstractClassMethods) { visit(); }
    public void visit(AbstractClassMethodsOne AbstractClassMethodsOne) { visit(); }
    public void visit(AbstractClassDecl AbstractClassDecl) { visit(); }
    public void visit(NoMethodDeclNull NoMethodDeclNull) { visit(); }
    public void visit(MethodDeclNullOne MethodDeclNullOne) { visit(); }
    public void visit(NoExtends NoExtends) { visit(); }
    public void visit(ExtendsError ExtendsError) { visit(); }
    public void visit(ExtendsErrorMultiple ExtendsErrorMultiple) { visit(); }
    public void visit(ExtendsCorrect ExtendsCorrect) { visit(); }
    public void visit(ClassDecl ClassDecl) { visit(); }
    public void visit(Lequal Lequal) { visit(); }
    public void visit(Low Low) { visit(); }
    public void visit(Gequal Gequal) { visit(); }
    public void visit(Great Great) { visit(); }
    public void visit(Nequal Nequal) { visit(); }
    public void visit(Equal Equal) { visit(); }
    public void visit(CondFactOne CondFactOne) { visit(); }
    public void visit(CondFactRelop CondFactRelop) { visit(); }
    public void visit(CondTermOne CondTermOne) { visit(); }
    public void visit(CondTermAnd CondTermAnd) { visit(); }
    public void visit(ConditionOne ConditionOne) { visit(); }
    public void visit(ConditionOR ConditionOR) { visit(); }
    public void visit(Minus Minus) { visit(); }
    public void visit(Plus Plus) { visit(); }
    public void visit(Assignop Assignop) { visit(); }
    public void visit(ActualParamListExpr ActualParamListExpr) { visit(); }
    public void visit(ActualParamListComma ActualParamListComma) { visit(); }
    public void visit(NoActualPars NoActualPars) { visit(); }
    public void visit(ActualParsOne ActualParsOne) { visit(); }
    public void visit(FactorFuncCall FactorFuncCall) { visit(); }
    public void visit(FactorDesignator FactorDesignator) { visit(); }
    public void visit(FactorParen FactorParen) { visit(); }
    public void visit(FactorNew FactorNew) { visit(); }
    public void visit(FactorNewSquare FactorNewSquare) { visit(); }
    public void visit(FactorBoolConst FactorBoolConst) { visit(); }
    public void visit(FactorPrintChar FactorPrintChar) { visit(); }
    public void visit(FactorNumConst FactorNumConst) { visit(); }
    public void visit(Mod Mod) { visit(); }
    public void visit(Div Div) { visit(); }
    public void visit(MulOperation MulOperation) { visit(); }
    public void visit(TermFactor TermFactor) { visit(); }
    public void visit(TermMulop TermMulop) { visit(); }
    public void visit(ExprAddop ExprAddop) { visit(); }
    public void visit(ExprTerm ExprTerm) { visit(); }
    public void visit(ExprMinusTerm ExprMinusTerm) { visit(); }
    public void visit(DesignatorSquare DesignatorSquare) { visit(); }
    public void visit(DesignatorPoint DesignatorPoint) { visit(); }
    public void visit(DesignatorIdent DesignatorIdent) { visit(); }
    public void visit(DesignatorDec DesignatorDec) { visit(); }
    public void visit(DesignatorInc DesignatorInc) { visit(); }
    public void visit(FuncCall FuncCall) { visit(); }
    public void visit(AssignmentError AssignmentError) { visit(); }
    public void visit(Assignment Assignment) { visit(); }
    public void visit(NoForConditionNULL NoForConditionNULL) { visit(); }
    public void visit(ForConditionNULLError ForConditionNULLError) { visit(); }
    public void visit(ForConditionNotNULL ForConditionNotNULL) { visit(); }
    public void visit(NoDesignatorStatementNull NoDesignatorStatementNull) { visit(); }
    public void visit(DesignatorStatemenOne DesignatorStatemenOne) { visit(); }
    public void visit(EpsilonHelp EpsilonHelp) { visit(); }
    public void visit(MatchedFor MatchedFor) { visit(); }
    public void visit(MatchedStatementList MatchedStatementList) { visit(); }
    public void visit(MatchedIfElse MatchedIfElse) { visit(); }
    public void visit(MatchedDesignatorStatement MatchedDesignatorStatement) { visit(); }
    public void visit(MatchedContinue MatchedContinue) { visit(); }
    public void visit(MatchedBreak MatchedBreak) { visit(); }
    public void visit(MatchedReturn MatchedReturn) { visit(); }
    public void visit(MatchedReturnExpr MatchedReturnExpr) { visit(); }
    public void visit(MatchedRead MatchedRead) { visit(); }
    public void visit(MatchedPrintNumConst MatchedPrintNumConst) { visit(); }
    public void visit(MatchedPrint MatchedPrint) { visit(); }
    public void visit(UnmatchedFor UnmatchedFor) { visit(); }
    public void visit(UnmatchedIfElse UnmatchedIfElse) { visit(); }
    public void visit(UnmatchedIf UnmatchedIf) { visit(); }
    public void visit(StatementUnmatched StatementUnmatched) { visit(); }
    public void visit(StatementMatched StatementMatched) { visit(); }
    public void visit(NoStatementList NoStatementList) { visit(); }
    public void visit(StatementListMultiple StatementListMultiple) { visit(); }
    public void visit(FormParsErrorComma FormParsErrorComma) { visit(); }
    public void visit(FormalParamDeclSquare FormalParamDeclSquare) { visit(); }
    public void visit(FormalParamDeclNoSquare FormalParamDeclNoSquare) { visit(); }
    public void visit(FormParsOne FormParsOne) { visit(); }
    public void visit(FormParsComma FormParsComma) { visit(); }
    public void visit(NoFormParsNULL NoFormParsNULL) { visit(); }
    public void visit(FormParsSmth FormParsSmth) { visit(); }
    public void visit(MethodTypeVoid MethodTypeVoid) { visit(); }
    public void visit(MethodTypeType MethodTypeType) { visit(); }
    public void visit(MethodTypeName MethodTypeName) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(NoMethodDeclList NoMethodDeclList) { visit(); }
    public void visit(MethodDeclListMultiple MethodDeclListMultiple) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(FalseBool FalseBool) { visit(); }
    public void visit(TrueBool TrueBool) { visit(); }
    public void visit(VarDeclElemError VarDeclElemError) { visit(); }
    public void visit(SquareVarDeclElem SquareVarDeclElem) { visit(); }
    public void visit(WithoutSquareVarDeclElem WithoutSquareVarDeclElem) { visit(); }
    public void visit(VarElemListOne VarElemListOne) { visit(); }
    public void visit(VarElemListComma VarElemListComma) { visit(); }
    public void visit(VarDecl VarDecl) { visit(); }
    public void visit(NoVarDeclList NoVarDeclList) { visit(); }
    public void visit(VarDeclListMultiple VarDeclListMultiple) { visit(); }
    public void visit(ConstDeclElemPrintChar ConstDeclElemPrintChar) { visit(); }
    public void visit(ConstDeclElemBoolConst ConstDeclElemBoolConst) { visit(); }
    public void visit(ConstDeclElemNumConst ConstDeclElemNumConst) { visit(); }
    public void visit(ConstElemListOne ConstElemListOne) { visit(); }
    public void visit(ConstElemListComma ConstElemListComma) { visit(); }
    public void visit(ConstDecl ConstDecl) { visit(); }
    public void visit(NoDeclarationLists NoDeclarationLists) { visit(); }
    public void visit(DeclarationListsClass DeclarationListsClass) { visit(); }
    public void visit(DeclarationListsAbstractClass DeclarationListsAbstractClass) { visit(); }
    public void visit(DeclarationListsConst DeclarationListsConst) { visit(); }
    public void visit(DeclarationListsVar DeclarationListsVar) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
