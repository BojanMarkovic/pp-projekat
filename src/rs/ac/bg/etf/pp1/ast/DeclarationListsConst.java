// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class DeclarationListsConst extends DeclarationLists {

    private DeclarationLists DeclarationLists;
    private ConstDecl ConstDecl;

    public DeclarationListsConst (DeclarationLists DeclarationLists, ConstDecl ConstDecl) {
        this.DeclarationLists=DeclarationLists;
        if(DeclarationLists!=null) DeclarationLists.setParent(this);
        this.ConstDecl=ConstDecl;
        if(ConstDecl!=null) ConstDecl.setParent(this);
    }

    public DeclarationLists getDeclarationLists() {
        return DeclarationLists;
    }

    public void setDeclarationLists(DeclarationLists DeclarationLists) {
        this.DeclarationLists=DeclarationLists;
    }

    public ConstDecl getConstDecl() {
        return ConstDecl;
    }

    public void setConstDecl(ConstDecl ConstDecl) {
        this.ConstDecl=ConstDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.accept(visitor);
        if(ConstDecl!=null) ConstDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DeclarationLists!=null) DeclarationLists.traverseTopDown(visitor);
        if(ConstDecl!=null) ConstDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.traverseBottomUp(visitor);
        if(ConstDecl!=null) ConstDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DeclarationListsConst(\n");

        if(DeclarationLists!=null)
            buffer.append(DeclarationLists.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDecl!=null)
            buffer.append(ConstDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DeclarationListsConst]");
        return buffer.toString();
    }
}
