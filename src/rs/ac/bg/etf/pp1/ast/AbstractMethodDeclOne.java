// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public class AbstractMethodDeclOne extends AbstractMethodDecl {

    private MethodType MethodType;
    private String I2;
    private FormParsNULL FormParsNULL;

    public AbstractMethodDeclOne (MethodType MethodType, String I2, FormParsNULL FormParsNULL) {
        this.MethodType=MethodType;
        if(MethodType!=null) MethodType.setParent(this);
        this.I2=I2;
        this.FormParsNULL=FormParsNULL;
        if(FormParsNULL!=null) FormParsNULL.setParent(this);
    }

    public MethodType getMethodType() {
        return MethodType;
    }

    public void setMethodType(MethodType MethodType) {
        this.MethodType=MethodType;
    }

    public String getI2() {
        return I2;
    }

    public void setI2(String I2) {
        this.I2=I2;
    }

    public FormParsNULL getFormParsNULL() {
        return FormParsNULL;
    }

    public void setFormParsNULL(FormParsNULL FormParsNULL) {
        this.FormParsNULL=FormParsNULL;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodType!=null) MethodType.accept(visitor);
        if(FormParsNULL!=null) FormParsNULL.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodType!=null) MethodType.traverseTopDown(visitor);
        if(FormParsNULL!=null) FormParsNULL.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodType!=null) MethodType.traverseBottomUp(visitor);
        if(FormParsNULL!=null) FormParsNULL.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractMethodDeclOne(\n");

        if(MethodType!=null)
            buffer.append(MethodType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+I2);
        buffer.append("\n");

        if(FormParsNULL!=null)
            buffer.append(FormParsNULL.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractMethodDeclOne]");
        return buffer.toString();
    }
}
