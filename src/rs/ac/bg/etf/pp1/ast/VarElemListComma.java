// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class VarElemListComma extends VarElemList {

    private VarDeclElem VarDeclElem;
    private VarElemList VarElemList;

    public VarElemListComma (VarDeclElem VarDeclElem, VarElemList VarElemList) {
        this.VarDeclElem=VarDeclElem;
        if(VarDeclElem!=null) VarDeclElem.setParent(this);
        this.VarElemList=VarElemList;
        if(VarElemList!=null) VarElemList.setParent(this);
    }

    public VarDeclElem getVarDeclElem() {
        return VarDeclElem;
    }

    public void setVarDeclElem(VarDeclElem VarDeclElem) {
        this.VarDeclElem=VarDeclElem;
    }

    public VarElemList getVarElemList() {
        return VarElemList;
    }

    public void setVarElemList(VarElemList VarElemList) {
        this.VarElemList=VarElemList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclElem!=null) VarDeclElem.accept(visitor);
        if(VarElemList!=null) VarElemList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclElem!=null) VarDeclElem.traverseTopDown(visitor);
        if(VarElemList!=null) VarElemList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclElem!=null) VarDeclElem.traverseBottomUp(visitor);
        if(VarElemList!=null) VarElemList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarElemListComma(\n");

        if(VarDeclElem!=null)
            buffer.append(VarDeclElem.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarElemList!=null)
            buffer.append(VarElemList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarElemListComma]");
        return buffer.toString();
    }
}
