// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class DeclarationListsClass extends DeclarationLists {

    private DeclarationLists DeclarationLists;
    private ClassDecl ClassDecl;

    public DeclarationListsClass (DeclarationLists DeclarationLists, ClassDecl ClassDecl) {
        this.DeclarationLists=DeclarationLists;
        if(DeclarationLists!=null) DeclarationLists.setParent(this);
        this.ClassDecl=ClassDecl;
        if(ClassDecl!=null) ClassDecl.setParent(this);
    }

    public DeclarationLists getDeclarationLists() {
        return DeclarationLists;
    }

    public void setDeclarationLists(DeclarationLists DeclarationLists) {
        this.DeclarationLists=DeclarationLists;
    }

    public ClassDecl getClassDecl() {
        return ClassDecl;
    }

    public void setClassDecl(ClassDecl ClassDecl) {
        this.ClassDecl=ClassDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.accept(visitor);
        if(ClassDecl!=null) ClassDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DeclarationLists!=null) DeclarationLists.traverseTopDown(visitor);
        if(ClassDecl!=null) ClassDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.traverseBottomUp(visitor);
        if(ClassDecl!=null) ClassDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DeclarationListsClass(\n");

        if(DeclarationLists!=null)
            buffer.append(DeclarationLists.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDecl!=null)
            buffer.append(ClassDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DeclarationListsClass]");
        return buffer.toString();
    }
}
