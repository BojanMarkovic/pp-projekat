// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class DeclarationListsVar extends DeclarationLists {

    private DeclarationLists DeclarationLists;
    private VarDecl VarDecl;

    public DeclarationListsVar (DeclarationLists DeclarationLists, VarDecl VarDecl) {
        this.DeclarationLists=DeclarationLists;
        if(DeclarationLists!=null) DeclarationLists.setParent(this);
        this.VarDecl=VarDecl;
        if(VarDecl!=null) VarDecl.setParent(this);
    }

    public DeclarationLists getDeclarationLists() {
        return DeclarationLists;
    }

    public void setDeclarationLists(DeclarationLists DeclarationLists) {
        this.DeclarationLists=DeclarationLists;
    }

    public VarDecl getVarDecl() {
        return VarDecl;
    }

    public void setVarDecl(VarDecl VarDecl) {
        this.VarDecl=VarDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.accept(visitor);
        if(VarDecl!=null) VarDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DeclarationLists!=null) DeclarationLists.traverseTopDown(visitor);
        if(VarDecl!=null) VarDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.traverseBottomUp(visitor);
        if(VarDecl!=null) VarDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DeclarationListsVar(\n");

        if(DeclarationLists!=null)
            buffer.append(DeclarationLists.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecl!=null)
            buffer.append(VarDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DeclarationListsVar]");
        return buffer.toString();
    }
}
