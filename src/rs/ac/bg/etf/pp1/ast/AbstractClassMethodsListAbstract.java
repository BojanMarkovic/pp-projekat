// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassMethodsListAbstract extends AbstractClassMethodsList {

    private AbstractClassMethodsList AbstractClassMethodsList;
    private AbstractMethodDecl AbstractMethodDecl;

    public AbstractClassMethodsListAbstract (AbstractClassMethodsList AbstractClassMethodsList, AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.setParent(this);
        this.AbstractMethodDecl=AbstractMethodDecl;
        if(AbstractMethodDecl!=null) AbstractMethodDecl.setParent(this);
    }

    public AbstractClassMethodsList getAbstractClassMethodsList() {
        return AbstractClassMethodsList;
    }

    public void setAbstractClassMethodsList(AbstractClassMethodsList AbstractClassMethodsList) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
    }

    public AbstractMethodDecl getAbstractMethodDecl() {
        return AbstractMethodDecl;
    }

    public void setAbstractMethodDecl(AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractMethodDecl=AbstractMethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.accept(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseTopDown(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseBottomUp(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassMethodsListAbstract(\n");

        if(AbstractClassMethodsList!=null)
            buffer.append(AbstractClassMethodsList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractMethodDecl!=null)
            buffer.append(AbstractMethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassMethodsListAbstract]");
        return buffer.toString();
    }
}
