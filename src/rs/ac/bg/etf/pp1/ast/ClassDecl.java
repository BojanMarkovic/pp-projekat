// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public class ClassDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private String I1;
    private ExtendsNull ExtendsNull;
    private VarDeclList VarDeclList;
    private MethodDeclNull MethodDeclNull;

    public ClassDecl (String I1, ExtendsNull ExtendsNull, VarDeclList VarDeclList, MethodDeclNull MethodDeclNull) {
        this.I1=I1;
        this.ExtendsNull=ExtendsNull;
        if(ExtendsNull!=null) ExtendsNull.setParent(this);
        this.VarDeclList=VarDeclList;
        if(VarDeclList!=null) VarDeclList.setParent(this);
        this.MethodDeclNull=MethodDeclNull;
        if(MethodDeclNull!=null) MethodDeclNull.setParent(this);
    }

    public String getI1() {
        return I1;
    }

    public void setI1(String I1) {
        this.I1=I1;
    }

    public ExtendsNull getExtendsNull() {
        return ExtendsNull;
    }

    public void setExtendsNull(ExtendsNull ExtendsNull) {
        this.ExtendsNull=ExtendsNull;
    }

    public VarDeclList getVarDeclList() {
        return VarDeclList;
    }

    public void setVarDeclList(VarDeclList VarDeclList) {
        this.VarDeclList=VarDeclList;
    }

    public MethodDeclNull getMethodDeclNull() {
        return MethodDeclNull;
    }

    public void setMethodDeclNull(MethodDeclNull MethodDeclNull) {
        this.MethodDeclNull=MethodDeclNull;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ExtendsNull!=null) ExtendsNull.accept(visitor);
        if(VarDeclList!=null) VarDeclList.accept(visitor);
        if(MethodDeclNull!=null) MethodDeclNull.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ExtendsNull!=null) ExtendsNull.traverseTopDown(visitor);
        if(VarDeclList!=null) VarDeclList.traverseTopDown(visitor);
        if(MethodDeclNull!=null) MethodDeclNull.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ExtendsNull!=null) ExtendsNull.traverseBottomUp(visitor);
        if(VarDeclList!=null) VarDeclList.traverseBottomUp(visitor);
        if(MethodDeclNull!=null) MethodDeclNull.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDecl(\n");

        buffer.append(" "+tab+I1);
        buffer.append("\n");

        if(ExtendsNull!=null)
            buffer.append(ExtendsNull.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclList!=null)
            buffer.append(VarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDeclNull!=null)
            buffer.append(MethodDeclNull.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDecl]");
        return buffer.toString();
    }
}
