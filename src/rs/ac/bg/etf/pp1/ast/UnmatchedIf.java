// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class UnmatchedIf extends Unmatched {

    private EpsilonHelp EpsilonHelp;
    private Condition Condition;
    private Statement Statement;

    public UnmatchedIf (EpsilonHelp EpsilonHelp, Condition Condition, Statement Statement) {
        this.EpsilonHelp=EpsilonHelp;
        if(EpsilonHelp!=null) EpsilonHelp.setParent(this);
        this.Condition=Condition;
        if(Condition!=null) Condition.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
    }

    public EpsilonHelp getEpsilonHelp() {
        return EpsilonHelp;
    }

    public void setEpsilonHelp(EpsilonHelp EpsilonHelp) {
        this.EpsilonHelp=EpsilonHelp;
    }

    public Condition getCondition() {
        return Condition;
    }

    public void setCondition(Condition Condition) {
        this.Condition=Condition;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.accept(visitor);
        if(Condition!=null) Condition.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EpsilonHelp!=null) EpsilonHelp.traverseTopDown(visitor);
        if(Condition!=null) Condition.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.traverseBottomUp(visitor);
        if(Condition!=null) Condition.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("UnmatchedIf(\n");

        if(EpsilonHelp!=null)
            buffer.append(EpsilonHelp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Condition!=null)
            buffer.append(Condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [UnmatchedIf]");
        return buffer.toString();
    }
}
