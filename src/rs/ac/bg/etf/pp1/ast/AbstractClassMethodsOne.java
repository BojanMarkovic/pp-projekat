// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassMethodsOne extends AbstractClassMethods {

    private AbstractClassMethodsList AbstractClassMethodsList;

    public AbstractClassMethodsOne (AbstractClassMethodsList AbstractClassMethodsList) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.setParent(this);
    }

    public AbstractClassMethodsList getAbstractClassMethodsList() {
        return AbstractClassMethodsList;
    }

    public void setAbstractClassMethodsList(AbstractClassMethodsList AbstractClassMethodsList) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassMethodsOne(\n");

        if(AbstractClassMethodsList!=null)
            buffer.append(AbstractClassMethodsList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassMethodsOne]");
        return buffer.toString();
    }
}
