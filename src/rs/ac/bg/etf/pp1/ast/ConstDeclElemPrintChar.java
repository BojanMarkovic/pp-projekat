// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclElemPrintChar extends ConstDeclElem {

    private String constName;
    private String charval;

    public ConstDeclElemPrintChar (String constName, String charval) {
        this.constName=constName;
        this.charval=charval;
    }

    public String getConstName() {
        return constName;
    }

    public void setConstName(String constName) {
        this.constName=constName;
    }

    public String getCharval() {
        return charval;
    }

    public void setCharval(String charval) {
        this.charval=charval;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclElemPrintChar(\n");

        buffer.append(" "+tab+constName);
        buffer.append("\n");

        buffer.append(" "+tab+charval);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclElemPrintChar]");
        return buffer.toString();
    }
}
