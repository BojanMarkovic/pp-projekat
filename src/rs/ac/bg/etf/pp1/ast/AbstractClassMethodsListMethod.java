// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:58


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassMethodsListMethod extends AbstractClassMethodsList {

    private AbstractClassMethodsList AbstractClassMethodsList;
    private MethodDecl MethodDecl;

    public AbstractClassMethodsListMethod (AbstractClassMethodsList AbstractClassMethodsList, MethodDecl MethodDecl) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.setParent(this);
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
    }

    public AbstractClassMethodsList getAbstractClassMethodsList() {
        return AbstractClassMethodsList;
    }

    public void setAbstractClassMethodsList(AbstractClassMethodsList AbstractClassMethodsList) {
        this.AbstractClassMethodsList=AbstractClassMethodsList;
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.accept(visitor);
        if(MethodDecl!=null) MethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseTopDown(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassMethodsList!=null) AbstractClassMethodsList.traverseBottomUp(visitor);
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassMethodsListMethod(\n");

        if(AbstractClassMethodsList!=null)
            buffer.append(AbstractClassMethodsList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassMethodsListMethod]");
        return buffer.toString();
    }
}
