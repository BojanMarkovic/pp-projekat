// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class MatchedIfElse extends Matched {

    private EpsilonHelp EpsilonHelp;
    private Condition Condition;
    private Matched Matched;
    private EpsilonHelp EpsilonHelp1;
    private Matched Matched2;

    public MatchedIfElse (EpsilonHelp EpsilonHelp, Condition Condition, Matched Matched, EpsilonHelp EpsilonHelp1, Matched Matched2) {
        this.EpsilonHelp=EpsilonHelp;
        if(EpsilonHelp!=null) EpsilonHelp.setParent(this);
        this.Condition=Condition;
        if(Condition!=null) Condition.setParent(this);
        this.Matched=Matched;
        if(Matched!=null) Matched.setParent(this);
        this.EpsilonHelp1=EpsilonHelp1;
        if(EpsilonHelp1!=null) EpsilonHelp1.setParent(this);
        this.Matched2=Matched2;
        if(Matched2!=null) Matched2.setParent(this);
    }

    public EpsilonHelp getEpsilonHelp() {
        return EpsilonHelp;
    }

    public void setEpsilonHelp(EpsilonHelp EpsilonHelp) {
        this.EpsilonHelp=EpsilonHelp;
    }

    public Condition getCondition() {
        return Condition;
    }

    public void setCondition(Condition Condition) {
        this.Condition=Condition;
    }

    public Matched getMatched() {
        return Matched;
    }

    public void setMatched(Matched Matched) {
        this.Matched=Matched;
    }

    public EpsilonHelp getEpsilonHelp1() {
        return EpsilonHelp1;
    }

    public void setEpsilonHelp1(EpsilonHelp EpsilonHelp1) {
        this.EpsilonHelp1=EpsilonHelp1;
    }

    public Matched getMatched2() {
        return Matched2;
    }

    public void setMatched2(Matched Matched2) {
        this.Matched2=Matched2;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.accept(visitor);
        if(Condition!=null) Condition.accept(visitor);
        if(Matched!=null) Matched.accept(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.accept(visitor);
        if(Matched2!=null) Matched2.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EpsilonHelp!=null) EpsilonHelp.traverseTopDown(visitor);
        if(Condition!=null) Condition.traverseTopDown(visitor);
        if(Matched!=null) Matched.traverseTopDown(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.traverseTopDown(visitor);
        if(Matched2!=null) Matched2.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.traverseBottomUp(visitor);
        if(Condition!=null) Condition.traverseBottomUp(visitor);
        if(Matched!=null) Matched.traverseBottomUp(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.traverseBottomUp(visitor);
        if(Matched2!=null) Matched2.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MatchedIfElse(\n");

        if(EpsilonHelp!=null)
            buffer.append(EpsilonHelp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Condition!=null)
            buffer.append(Condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched!=null)
            buffer.append(Matched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(EpsilonHelp1!=null)
            buffer.append(EpsilonHelp1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched2!=null)
            buffer.append(Matched2.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MatchedIfElse]");
        return buffer.toString();
    }
}
