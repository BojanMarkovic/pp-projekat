// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class DeclarationListsAbstractClass extends DeclarationLists {

    private DeclarationLists DeclarationLists;
    private AbstractClassDecl AbstractClassDecl;

    public DeclarationListsAbstractClass (DeclarationLists DeclarationLists, AbstractClassDecl AbstractClassDecl) {
        this.DeclarationLists=DeclarationLists;
        if(DeclarationLists!=null) DeclarationLists.setParent(this);
        this.AbstractClassDecl=AbstractClassDecl;
        if(AbstractClassDecl!=null) AbstractClassDecl.setParent(this);
    }

    public DeclarationLists getDeclarationLists() {
        return DeclarationLists;
    }

    public void setDeclarationLists(DeclarationLists DeclarationLists) {
        this.DeclarationLists=DeclarationLists;
    }

    public AbstractClassDecl getAbstractClassDecl() {
        return AbstractClassDecl;
    }

    public void setAbstractClassDecl(AbstractClassDecl AbstractClassDecl) {
        this.AbstractClassDecl=AbstractClassDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.accept(visitor);
        if(AbstractClassDecl!=null) AbstractClassDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DeclarationLists!=null) DeclarationLists.traverseTopDown(visitor);
        if(AbstractClassDecl!=null) AbstractClassDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DeclarationLists!=null) DeclarationLists.traverseBottomUp(visitor);
        if(AbstractClassDecl!=null) AbstractClassDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DeclarationListsAbstractClass(\n");

        if(DeclarationLists!=null)
            buffer.append(DeclarationLists.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractClassDecl!=null)
            buffer.append(AbstractClassDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DeclarationListsAbstractClass]");
        return buffer.toString();
    }
}
