// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclElemBoolConst extends ConstDeclElem {

    private String constName;
    private Boolconst Boolconst;

    public ConstDeclElemBoolConst (String constName, Boolconst Boolconst) {
        this.constName=constName;
        this.Boolconst=Boolconst;
        if(Boolconst!=null) Boolconst.setParent(this);
    }

    public String getConstName() {
        return constName;
    }

    public void setConstName(String constName) {
        this.constName=constName;
    }

    public Boolconst getBoolconst() {
        return Boolconst;
    }

    public void setBoolconst(Boolconst Boolconst) {
        this.Boolconst=Boolconst;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Boolconst!=null) Boolconst.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Boolconst!=null) Boolconst.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Boolconst!=null) Boolconst.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclElemBoolConst(\n");

        buffer.append(" "+tab+constName);
        buffer.append("\n");

        if(Boolconst!=null)
            buffer.append(Boolconst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclElemBoolConst]");
        return buffer.toString();
    }
}
