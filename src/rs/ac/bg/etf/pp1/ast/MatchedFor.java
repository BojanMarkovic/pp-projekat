// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class MatchedFor extends Matched {

    private EpsilonHelp EpsilonHelp;
    private DesignatorStatementNULL DesignatorStatementNULL;
    private ForConditionNULL ForConditionNULL;
    private DesignatorStatementNULL DesignatorStatementNULL1;
    private Matched Matched;

    public MatchedFor (EpsilonHelp EpsilonHelp, DesignatorStatementNULL DesignatorStatementNULL, ForConditionNULL ForConditionNULL, DesignatorStatementNULL DesignatorStatementNULL1, Matched Matched) {
        this.EpsilonHelp=EpsilonHelp;
        if(EpsilonHelp!=null) EpsilonHelp.setParent(this);
        this.DesignatorStatementNULL=DesignatorStatementNULL;
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.setParent(this);
        this.ForConditionNULL=ForConditionNULL;
        if(ForConditionNULL!=null) ForConditionNULL.setParent(this);
        this.DesignatorStatementNULL1=DesignatorStatementNULL1;
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.setParent(this);
        this.Matched=Matched;
        if(Matched!=null) Matched.setParent(this);
    }

    public EpsilonHelp getEpsilonHelp() {
        return EpsilonHelp;
    }

    public void setEpsilonHelp(EpsilonHelp EpsilonHelp) {
        this.EpsilonHelp=EpsilonHelp;
    }

    public DesignatorStatementNULL getDesignatorStatementNULL() {
        return DesignatorStatementNULL;
    }

    public void setDesignatorStatementNULL(DesignatorStatementNULL DesignatorStatementNULL) {
        this.DesignatorStatementNULL=DesignatorStatementNULL;
    }

    public ForConditionNULL getForConditionNULL() {
        return ForConditionNULL;
    }

    public void setForConditionNULL(ForConditionNULL ForConditionNULL) {
        this.ForConditionNULL=ForConditionNULL;
    }

    public DesignatorStatementNULL getDesignatorStatementNULL1() {
        return DesignatorStatementNULL1;
    }

    public void setDesignatorStatementNULL1(DesignatorStatementNULL DesignatorStatementNULL1) {
        this.DesignatorStatementNULL1=DesignatorStatementNULL1;
    }

    public Matched getMatched() {
        return Matched;
    }

    public void setMatched(Matched Matched) {
        this.Matched=Matched;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.accept(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.accept(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.accept(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.accept(visitor);
        if(Matched!=null) Matched.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EpsilonHelp!=null) EpsilonHelp.traverseTopDown(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.traverseTopDown(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.traverseTopDown(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.traverseTopDown(visitor);
        if(Matched!=null) Matched.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.traverseBottomUp(visitor);
        if(DesignatorStatementNULL!=null) DesignatorStatementNULL.traverseBottomUp(visitor);
        if(ForConditionNULL!=null) ForConditionNULL.traverseBottomUp(visitor);
        if(DesignatorStatementNULL1!=null) DesignatorStatementNULL1.traverseBottomUp(visitor);
        if(Matched!=null) Matched.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MatchedFor(\n");

        if(EpsilonHelp!=null)
            buffer.append(EpsilonHelp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementNULL!=null)
            buffer.append(DesignatorStatementNULL.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForConditionNULL!=null)
            buffer.append(ForConditionNULL.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorStatementNULL1!=null)
            buffer.append(DesignatorStatementNULL1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched!=null)
            buffer.append(Matched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MatchedFor]");
        return buffer.toString();
    }
}
