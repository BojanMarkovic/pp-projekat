// generated with ast extension for cup
// version 0.8
// 2/1/2020 10:34:57


package rs.ac.bg.etf.pp1.ast;

public class UnmatchedIfElse extends Unmatched {

    private EpsilonHelp EpsilonHelp;
    private Condition Condition;
    private Matched Matched;
    private EpsilonHelp EpsilonHelp1;
    private Unmatched Unmatched;

    public UnmatchedIfElse (EpsilonHelp EpsilonHelp, Condition Condition, Matched Matched, EpsilonHelp EpsilonHelp1, Unmatched Unmatched) {
        this.EpsilonHelp=EpsilonHelp;
        if(EpsilonHelp!=null) EpsilonHelp.setParent(this);
        this.Condition=Condition;
        if(Condition!=null) Condition.setParent(this);
        this.Matched=Matched;
        if(Matched!=null) Matched.setParent(this);
        this.EpsilonHelp1=EpsilonHelp1;
        if(EpsilonHelp1!=null) EpsilonHelp1.setParent(this);
        this.Unmatched=Unmatched;
        if(Unmatched!=null) Unmatched.setParent(this);
    }

    public EpsilonHelp getEpsilonHelp() {
        return EpsilonHelp;
    }

    public void setEpsilonHelp(EpsilonHelp EpsilonHelp) {
        this.EpsilonHelp=EpsilonHelp;
    }

    public Condition getCondition() {
        return Condition;
    }

    public void setCondition(Condition Condition) {
        this.Condition=Condition;
    }

    public Matched getMatched() {
        return Matched;
    }

    public void setMatched(Matched Matched) {
        this.Matched=Matched;
    }

    public EpsilonHelp getEpsilonHelp1() {
        return EpsilonHelp1;
    }

    public void setEpsilonHelp1(EpsilonHelp EpsilonHelp1) {
        this.EpsilonHelp1=EpsilonHelp1;
    }

    public Unmatched getUnmatched() {
        return Unmatched;
    }

    public void setUnmatched(Unmatched Unmatched) {
        this.Unmatched=Unmatched;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.accept(visitor);
        if(Condition!=null) Condition.accept(visitor);
        if(Matched!=null) Matched.accept(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.accept(visitor);
        if(Unmatched!=null) Unmatched.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EpsilonHelp!=null) EpsilonHelp.traverseTopDown(visitor);
        if(Condition!=null) Condition.traverseTopDown(visitor);
        if(Matched!=null) Matched.traverseTopDown(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.traverseTopDown(visitor);
        if(Unmatched!=null) Unmatched.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EpsilonHelp!=null) EpsilonHelp.traverseBottomUp(visitor);
        if(Condition!=null) Condition.traverseBottomUp(visitor);
        if(Matched!=null) Matched.traverseBottomUp(visitor);
        if(EpsilonHelp1!=null) EpsilonHelp1.traverseBottomUp(visitor);
        if(Unmatched!=null) Unmatched.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("UnmatchedIfElse(\n");

        if(EpsilonHelp!=null)
            buffer.append(EpsilonHelp.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Condition!=null)
            buffer.append(Condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Matched!=null)
            buffer.append(Matched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(EpsilonHelp1!=null)
            buffer.append(EpsilonHelp1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Unmatched!=null)
            buffer.append(Unmatched.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [UnmatchedIfElse]");
        return buffer.toString();
    }
}
