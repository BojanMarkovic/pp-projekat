package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;

%%

%{
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type) {
		return new Symbol(type, yyline+1, yycolumn);
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type, Object value) {
		return new Symbol(type, yyline+1, yycolumn, value);
	}

%}

%cup
%line
%column

%state COMMENT

%eofval{
	return new_symbol(sym.EOF);
%eofval}

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]

Comment  = "//" {InputCharacter}* {LineTerminator}?


%%

	" " 		{ }
	"\b" 		{ }
	"\t" 		{ }
	"\r\n" 		{ }
	"\f" 		{ }
	{Comment}   { }
	
	"abstract"		{ return new_symbol(sym.ABSTRACT, yytext()); }
	"class"			{ return new_symbol(sym.CLASS, yytext()); }
	"extends"		{ return new_symbol(sym.EXTENDS, yytext()); }
	"program"   	{ return new_symbol(sym.PROGRAM, yytext()); }
	"break" 		{ return new_symbol(sym.BREAK, yytext()); }
	"else" 			{ return new_symbol(sym.ELSE, yytext()); }
	"const" 		{ return new_symbol(sym.CONST, yytext()); }
	"if" 			{ return new_symbol(sym.IF, yytext()); }
	"new" 			{ return new_symbol(sym.NEW, yytext()); }
	"print" 		{ return new_symbol(sym.PRINT, yytext()); }
	"read" 			{ return new_symbol(sym.READ, yytext()); }
	"return" 		{ return new_symbol(sym.RETURN, yytext()); }
	"void" 			{ return new_symbol(sym.VOID, yytext()); }
	"for" 			{ return new_symbol(sym.FOR, yytext()); }
	"continue" 		{ return new_symbol(sym.CONTINUE, yytext()); }
	"+" 			{ return new_symbol(sym.PLUS, yytext()); }
	"-" 			{ return new_symbol(sym.MINUS, yytext()); }
	"*" 			{ return new_symbol(sym.MUL, yytext()); }
	"/" 			{ return new_symbol(sym.DIV, yytext()); }
	"%" 			{ return new_symbol(sym.MOD, yytext()); }
	"==" 			{ return new_symbol(sym.EQUAL, yytext()); }
	"!=" 			{ return new_symbol(sym.NEQUAL, yytext()); }
	">" 			{ return new_symbol(sym.GREAT, yytext()); }
	">=" 			{ return new_symbol(sym.GEQUAL, yytext()); }
	"<" 			{ return new_symbol(sym.LOW, yytext()); }
	"<=" 			{ return new_symbol(sym.LEQUAL, yytext()); }
	"&&" 			{ return new_symbol(sym.AND, yytext()); }
	"||" 			{ return new_symbol(sym.OR, yytext()); }
	"=" 			{ return new_symbol(sym.ASSIGN, yytext()); }
	"++" 			{ return new_symbol(sym.INC, yytext()); }
	"--" 			{ return new_symbol(sym.DEC, yytext()); }
	";" 			{ return new_symbol(sym.SEMI, yytext()); }
	"," 			{ return new_symbol(sym.COMMA, yytext()); }
	"." 			{ return new_symbol(sym.POINT, yytext()); }
	"(" 			{ return new_symbol(sym.LPAREN, yytext()); }
	")" 			{ return new_symbol(sym.RPAREN, yytext()); }
	"{" 			{ return new_symbol(sym.LBRACE, yytext()); }
	"}"				{ return new_symbol(sym.RBRACE, yytext()); }
	"[" 			{ return new_symbol(sym.LSQUARE, yytext()); }
	"]"				{ return new_symbol(sym.RSQUARE, yytext()); }
	"."				{ return new_symbol(sym.POINT, yytext()); }
	"true"			{ return new_symbol (sym.TRUE, yytext()); }
	"false"         { return new_symbol (sym.FALSE, yytext()); }
	
	(0 | [1-9][0-9]*)  { return new_symbol(sym.NUMCONST, new Integer (yytext())); }
	(\'.\') { return new_symbol(sym.PRINTCHAR, new String(yytext())); }
	([a-z]|[A-Z])[a-z|A-Z|0-9|_]* 	{return new_symbol (sym.IDENT, yytext()); }
	
	. { System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1)+ ", u koloni " + (yycolumn)); }


